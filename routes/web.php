<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.home');
});

Route::get('/dashboard', function () {
	return view('pages.dashboard');
});

Route::get('/login', function () {
	return view('pages.login');
});

Route::get('/form', function () {
	return view('pages.form');
});

Route::get('/tables', function () {
	return view('pages.tables');
});

Route::get('/flot-charts', function () {
	return view('pages.flot-charts');
});

Route::get('/morris-charts', function () {
	return view('pages.morris-charts');
});

Route::get('/panels-wells', function () {
	return view('pages.panels-wells');
});

Route::get('/buttons', function () {
	return view('pages.buttons');
});

Route::get('/notifications', function () {
	return view('pages.notifications');
});

Route::get('/typography', function () {
	return view('pages.typography');
});

Route::get('/icons', function () {
	return view('pages.icons');
});

Route::get('/grid', function () {
	return view('pages.grid');
});
